import { TestBed, async } from '@angular/core/testing';
import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FormCollapsibleComponent} from './form-collapsible.component';
import {TranslateModule} from '@ngx-translate/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'universis-test-root',
    template: `<div>
        <form-collapsible [collapsed]="false">
            <form-header>Form title</form-header>
            <form-body>
                <form class="mt-2 d-none collapsible-form">
                    <div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group text-dark">
                                    <label class="text-dark" for="firstName">First Name</label>
                                    <input disabled="disabled"
                                           class="form-control bg-white text-secondary"
                                           id="firstName" type="text" placeholder="Enter your first name" value="Chelsea">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group text-dark">
                                    <label class="text-dark" for="lastName">Last Name</label>
                                    <input disabled="disabled"
                                           class="form-control bg-white text-secondary"
                                           id="lastName" type="text" placeholder="Enter your last name" value="Ross">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </form-body>
        </form-collapsible>
    </div>`
})
// ts-ignore
export class TestAppComponent {
    title = 'registrar';
}


describe('FormCollapsibleComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                FormsModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                TestAppComponent,
                FormCollapsibleComponent
            ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ]
        }).compileComponents();
    }));

    it('should get collapsible form header', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('form-header').textContent).toContain('Form title');
    });

    it('should get collapsible form body', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = <HTMLDivElement>fixture.debugElement.nativeElement;
        expect(compiled.querySelector('form')).toBeTruthy();
    });

    it('should toggle form', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = <HTMLDivElement>fixture.debugElement.nativeElement;
        expect(compiled.querySelector('form')).toBeTruthy();
        compiled.querySelector('a').click();
        fixture.detectChanges();
        expect(compiled.querySelector('form')).toBeFalsy();
    });

});
